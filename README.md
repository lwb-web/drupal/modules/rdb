# RDB - Erweiterungsmodul zur Rechtsdatenbank

Das Modul bildet kleine Erweiterungen ab, die auch mit [Rules](https://www.drupal.org/project/rules) abgebildet werden könnten. Aufgrund der Mächtigkeit von Rules ist eine eigene Implementation einfacher.

Instanzen werden über [Paragraphs](https://www.drupal.org/project/paragraphs) abgebildet.

## Features

* Link zum Schließen eines Prozesses: setzt das Statusfeld auf geschlossen
* Link zum Löschen eines Prozesses: setzt das Statusfeld auf gelöscht
* Statusfeld ist duch den Anwender nicht zu ändern
* Stellt Titelvoschläge für Prozesse und Instanzen bereit
* Löscht Prozesse zum 31.12. mit Status gelöscht oder abgeschlossen und die vor 10 Jahren angelegt wurden

## Berechtigungen

* rdb delete process: Prozesse löschen 
* rdb close process: Prozesse schließen

## Voraussetzungen

* [Drupal 8](https://www.drupal.org/8)
* [Paragraphs](https://www.drupal.org/project/paragraphs)
