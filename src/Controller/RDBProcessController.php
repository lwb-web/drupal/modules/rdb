<?php

namespace Drupal\rdb\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * Contains the BaseController controller.
 */
class RDBProcessController extends ControllerBase {
  private const RDB_PROCESS_CLOSE = 4;
  private const RDB_PROCESS_DELETE = 9;

  /**
   * Set RDB - Process status to close.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Current Node.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Returns a redirect to the node of the currently closed process.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function rdbClose(Node $node) {
    $node->set('field_rdb_process_category', self::RDB_PROCESS_CLOSE);
    $node->set('field_rdb_process_abgabe_archiv', date('Y-m-d'));
    $node->save();
    return $this->redirect('entity.node.canonical', [
      'node' => $node->id(),
    ]);
  }

  /**
   * Set RDB - Process status to delete.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Current Node.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Returns a redirect to the node of the currently deleted process.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function rdbDelete(Node $node) {
    $node->set('field_rdb_process_category', self::RDB_PROCESS_DELETE);
    $node->save();
    return $this->redirect('entity.node.canonical', [
      'node' => $node->id(),
    ]);
  }

  /**
   * Check Permission to show the RDB - Process Close-Tab.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Current Node.
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultNeutral
   *   Return Access result.
   */
  public function rdbCloseVisible(Node $node) {
    return AccessResult::allowedIf(
      $node->type->entity->id() === 'rdb_process' &&
      $node->get('field_rdb_process_category')->getString() < self::RDB_PROCESS_CLOSE
    );
  }

  /**
   * Check Permission to show the RDB - Process Close-Tab.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Current Node.
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultNeutral
   *   Return Access result.
   */
  public function rdbDeleteVisible(Node $node) {
    return AccessResult::allowedIf(
      $node->type->entity->id() === 'rdb_process' &&
      $node->get('field_rdb_process_category')->getString() < self::RDB_PROCESS_DELETE
    );
  }

}
